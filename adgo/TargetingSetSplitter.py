class TargetingSetSplitter:
    def __get_next_age(self, age, oldest):
        n = age + (10 - (age % 10))
        if n > oldest:
            n = oldest
        return n

    def __split_age_range(self, age_range):
        itr = self.__get_next_age(age_range[0], age_range[1])
        output = [[age_range[0], itr]]
        while itr < age_range[1]:
            prev = itr+1
            itr = self.__get_next_age(itr+1, age_range[1])
            output.append([prev, itr])
        return output

    def __get_target_set(self, country, placement, gender, age_range):
        output = {
            'countries': [country],
            'placements': [placement],
            'gender': gender,
            'ageRange': age_range
        }
        return output

    def split(self, target_set):
        output = []
        # TODO: assert each key exists
        # TODO: assert each var is the expected type

        # assert presence of necessary fields
        be_sure_str = "Be sure to add a {} field!"
        assert 'countries' in target_set, be_sure_str.format('countries')
        assert 'placement' in target_set, be_sure_str.format('placement')
        assert 'gender' in target_set, be_sure_str.format('gender')
        assert 'ageRange' in target_set, be_sure_str.format('ageRange')

        max_len_str = "{} contain at least 1 element"
        assert len(target_set['countries']) > 0, \
            max_len_str.format("'countries'")
        assert len(target_set['placement']) > 0, \
            max_len_str.format("'placement'")

        countries = target_set['countries']
        placement = target_set['placement']
        gender = target_set['gender']
        age_range = target_set['ageRange']

        # assert type of each field
        must_be_str = "{} must be a(n) {}"
        assert isinstance(countries, list), \
            must_be_str.format("'countries'", "list")
        assert isinstance(placement, list), \
            must_be_str.format("'placement'", "list")
        assert isinstance(gender, int), \
            must_be_str.format("'gender'", "integer")
        assert isinstance(age_range, list), \
            must_be_str.format("'age_range'", "list")

        # assert gender input
        assert gender >= 0 and gender <= 2, \
            "'gender' must be 0, 1, or 2 only"

        # get gender
        if gender == 2:
            gender = [0, 1]
        elif gender >= 0:
            gender = [gender]

        # assert ageRange input
        assert len(age_range) == 2, "'ageRange' can only contain 2 elements"
        assert age_range[0] < age_range[1], \
            "The first element of 'ageRange' must be lower than the second"
        # get age_set
        age_range = self.__split_age_range(age_range)

        # cringe
        for country in countries:
            for place in placement:
                for gen in gender:
                    for ar in age_range:
                        output.append(self.__get_target_set(
                                    country, place, gen, ar))

        return output

# for testing
if __name__ == '__main__':
    splitter = TargetingSetSplitter()
    input = {
        'countries': ['hk', 'jp'],
        'placement': ['desktop', 'mobile', 'external'],
        'gender': 0,
        'ageRange': [13, 15]
    }
    # worst case:
    '''
    input = {
        'countries': ['hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk', 'jp', 'hk'],
        'placement': ['desktop', 'mobile', 'external', 'desktop', 'mobile', 'external', 'desktop', 'mobile', 'external', 'desktop', 'mobile', 'external', 'desktop', 'mobile', 'external', 'desktop', 'mobile', 'external', 'desktop', 'mobile', 'external'],
        'gender': 2,
        'ageRange': [13, 64]
    }
    '''
    # print(splitter.split(input))
    # 80 * 21 * 6 * 2 = 20160
    # print(len(splitter.split(input)))

    print(splitter.split(input))

