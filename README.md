## Targeting Set Splitting

### Prerequisites
Python3.6

### Setup
- Copy the `adgo` directory to whereever you plan to test the code. From the same directory
where the `adgo` directory is located, you can import the TargetingSetSplitter class with
`from adgo.TargetingSetSplitter import TargetingSetSplitter`.

### Usage
```python
from adgo.TargetingSetSplitter import TargetingSetSplitter

splitter = TargetingSetSplitter()
sample_targetset = {
    'countries': ['hk', 'jp'],
    'placement': ['desktop', 'mobile', 'external'],
    'gender': 0,
    'ageRange': [13, 15]
}
splitter.split(sample_targetset)
```
