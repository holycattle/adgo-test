from adgo.TargetingSetSplitter import TargetingSetSplitter

splitter = TargetingSetSplitter()
sample_targetset = {
    'countries': ['hk', 'jp'],
    'placement': ['desktop', 'mobile', 'external'],
    'gender': 0,
    'ageRange': [13, 15]
}
print(splitter.split(sample_targetset))
